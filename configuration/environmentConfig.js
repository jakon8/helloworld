module.exports.loadEnvConfig = () => {
    if (process.env.NODE_ENV !== "production") {
        require('dotenv').config({ path: 'configuration/configs/dev_config.cfg' });
    } else {
        require('dotenv').config({ path: 'configuration/configs/prod_config.cfg' });
    }
};