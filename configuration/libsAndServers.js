const express = require("express");
const path = require("path");
const helmet = require("helmet");
const session = require("express-session");
const flash = require("connect-flash");
const ejsLayout = require('ejs-mate');
const mongoose = require('mongoose');
const mongoDBStore = require('connect-mongo');
const mongoSanitize = require('express-mongo-sanitize');
const coockieParser = require('cookie-parser');

async function connectToMongoDb() {

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error:"));
    db.once("open", () => {
        console.log(`CONNECTED to MONGO ${process.env.MONGODB_BASE} DB`);
    });

    await mongoose.connect(process.env.MONGODB_CONN + 'test', {});
}

/*const store = new mongoDBStore({

})*/

//przeniesc do osobnego pliku
const sessionConfig = {
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: {
        expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
        maxAge: 1000 * 60 * 60 * 24 * 7,
        httpOnly: true
    }
}

const mongoSanitizeConfig = {
    replaceWith: '_',
}
//

function setEnvPath() {
    process.env['PATH'] = process.env.ORACLE_CLIENT_PATH + ';' + process.env['PATH'];
}

function setUsings(app, dir) {
    app.engine('ejs', ejsLayout);
    app.set('view engine', 'ejs');
    app.set('views', path.join(dir, 'views'));
    app.use(express.static(path.join(dir, 'javascript')));
    app.use(express.static(path.join(dir, 'css')));
    app.use(express.urlencoded({ extended: true }));
    app.use(express.json());
    app.use(coockieParser(process.env.COOKIE_SECRET));
    app.use(helmet({ contentSecurityPolicy: false }));
    app.use(session(sessionConfig));
    app.use(flash());
    app.use(mongoSanitize(mongoSanitizeConfig),);
}

module.exports.loadsAndConfigs = (app, dir) => {
    setEnvPath();
    connectToMongoDb();
    setUsings(app, dir);
}

module.exports.startServer = (app) => {
    app.listen(process.env.SERVER_PORT, () => {
        console.log("LISTENING ON PORT 3000....")
    })
}