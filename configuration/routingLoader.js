const ExpressError = require('../tools/ExpressError');
const item_creators = require('../routes/item_creators');
const scheduler = require('../routes/scheduler');
const setup = require('../routes/setup');
const other = require('../routes/other');
const redmine = require('../routes/redmine');

const weirdFonts = require("weird-fonts");

module.exports.loadRoutigs = (app) => {
    app.use((req, res, next) => {
        res.locals.success = req.flash('success');
        res.locals.error = req.flash('error');
        next();
    })

    app.use('/itemcreators', item_creators);
    app.use('/scheduler', scheduler);
    app.use('/setup', setup);
    app.use('/other', other);
    app.use('/redmine', redmine);

    app.get('/', (req, res) => {
        res.render('start', { weirdFonts });
        //res.send(`<h1>${weirdFonts.circle("Hello World!")}</h1>`);
    })

    app.all('*', (req, res, next) => {
        req.flash('error', 'Ups... Page Not Found :)');
        res.redirect('/');
        //next(new ExpressError('Page Not Found!', 404));
    })

    app.use((err, req, res, next) => {
        const { statusCode = 500, message = 'Something went wrong' } = err;

        //res.status(statusCode).render('error', { error: err });
        //res.redirect('/');
    })
}