const express = require("express");
const router = express.Router();

router.get('/scheduler_jobs', (req, res) => {
    res.render(`setup/scheduler/scheduler_jobs`);
})

router.get('/scheduler_setup', (req, res) => {
    res.render(`setup/scheduler/scheduler_setup`);
})

module.exports = router;