const express = require("express");
const cachAsync = require('../tools/catchAsynch');
const itemCreatorController = require('../controllers/item_creators_controller');
const itemCreatorValidation = require('../controllers/validation/item_creators_validation');

const router = express.Router({ mergeParams: true });

/*router.get('/:creator', async (req, res) => {
    const { creator } = req.params;

    const data_cache = await cacheProvider.getProjects();

    res.render(`redmine/itemCreators/${creator}`, { data_cache });
})*/

//----------CUSTOM----------------
router.route('/custom_item')
    .get(cachAsync(itemCreatorController.renderCustomItem))
    .post(cachAsync(itemCreatorValidation.validateIssue), cachAsync(itemCreatorController.createCustomItem), cachAsync(itemCreatorController.createRegressionItem));

router.route('/custom_item/:proj_id')
    .get(cachAsync(itemCreatorController.getProjectMembers));

//-----------REGRESSION------------
router.route('/regression_item')
    .get(cachAsync(itemCreatorController.renderRegressionItem))
    .post(cachAsync(itemCreatorValidation.validateRegression), cachAsync(itemCreatorController.createRegressionItem));

router.route('/regression_item_create')
    .post(cachAsync(itemCreatorController.createNewRegressionItem))


//---------------------------------
router.get('/create_from_sd', async (req, res) => {
    res.render(`redmine/itemCreators/create_from_sd`);
})

router.get('/projects_item', async (req, res) => {
    res.render(`redmine/itemCreators/projects_item`);
})

router.get('/TMS_items', async (req, res) => {
    res.render(`redmine/itemCreators/TMS_items`);
})

module.exports = router;