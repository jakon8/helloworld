const express = require("express");
const setupController = require('../controllers/setup_controller');
const router = express.Router();

router.route('/setup_cache')
    .get(setupController.renderSetupCache)
    .post(setupController.clearCacheCt);

router.get('/setup_appearance', (req, res) => {
    res.render(`setup/setup_appearance`);
})

router.get('/setup_permissions', (req, res) => {
    res.render(`setup/setup_permissions`);
})

module.exports = router;