const express = require("express");
const router = express.Router();

router.get('/:oth', (req, res) => {
    const { oth } = req.params;
    res.render(`other/${oth}`);
})

module.exports = router;