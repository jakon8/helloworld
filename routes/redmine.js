const express = require("express");
const router = express.Router();

router.get('/item_synch', (req, res) => {
    res.render('redmine/synch_item');
})

router.get('/managers_dashboard', (req, res) => {
    res.render("redmine/managers_dashboard");
})

router.get('/daily_meetings', (req, res) => {
    res.render("redmine/daily_meetings");
})

module.exports = router;