const Joi = require('joi');

const ISSUE_REGEX = '^I|iS|sS|s-[a-zA-Z]+-\\d{1,6}[a-zA-Z]{2}$';
const CR_REGEX = '^c|Cr|R-[a-zA-Z]+-\\d{1,6}[a-zA-Z]{2}$';
const TMS_REGEX = '^[a-zA-Z]+-\\d{5}$';

module.exports.issueSchema = Joi.object({
    issue: Joi.object({
        project: Joi.string().required(),
        projectId: Joi.number().required(),
        tracker: Joi.number().required(),
        subject: Joi.string().required(),
        description: Joi.string().required(),
        assignee: Joi.string().required(),
        assigneeId: Joi.number().required(),
        sdIssue: Joi.string().pattern(new RegExp(ISSUE_REGEX)).allow('').messages({
            'string.pattern.base': `SD Issue# is in wrong format.`
        }),
        sdCR: Joi.string().pattern(new RegExp(CR_REGEX)).allow('').messages({
            'string.pattern.base': `SD CR# is in wrong format.`
        }),
        tms: Joi.string().pattern(new RegExp(TMS_REGEX)).allow('').messages({
            'string.pattern.base': `iTMS is in wrong format.`
        })
    }).required(),
    regression: Joi.object({
        project: Joi.string().required(),
        project_identifier: Joi.string().required(),
        projectId: Joi.number().required(),
        sdPacketId: Joi.number().required(),
        sdPacket: Joi.string().required(),
        display_created: Joi.string().required()
    }).optional()
});

module.exports.regressionSchema = Joi.object({
    regression: Joi.object({
        project: Joi.string().required(),
        project_identifier: Joi.string().required(),
        projectId: Joi.number().required(),
        sdPacketId: Joi.number().required(),
        sdPacket: Joi.string().required(),
        display_created: Joi.string().required()
    }).required()
});