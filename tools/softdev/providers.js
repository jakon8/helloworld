const oracledb = require('oracledb');
const queries = require('./queries');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

async function getOracleConnection() {
    /*const libPath = 'C:\\Oracle\\product\\instantclient_21_3';
    try {
        oracledb.initOracleClient({ libDir: libPath });
    } catch (err) {
        console.error('Problem with initOracleClient!');
        console.error(err);
    }*/
    return await oracledb.getConnection(
        {
            user: process.env.ORACLE_USER,
            password: process.env.ORACLE_PASS,
            connectString: process.env.ORACLE_CONN_STRING
        }//,
        //connExecute
    );
}

module.exports.getOracleConnection;

module.exports.getSDProjects = async () => {
    let conn;

    try {
        conn = await getOracleConnection();

        const result = await conn.execute(queries.getProjectsQuery());
        const objRet = result.rows;
        return objRet.sort((a, b) => a.PRODUCT_VERSION_NAME.localeCompare(b.PRODUCT_VERSION_NAME));
    } catch (err) {
        console.log('Error occurred', err);
    } finally {
        if (conn) {
            try {
                await conn.close();
                console.log('Connection closed');
            } catch (err) {
                console.log('Error closing connection', err);
            }
        }
    }
}

module.exports.getRegressionIssues = async (project = '', isPacket = false) => {
    let conn;

    try {
        conn = await getOracleConnection();
        //console.log(`project: ${project}, isPacket: ${isPacket}`);
        const result = await conn.execute(queries.getRegressionByProjectQuery(project, isPacket));
        const objRet = result.rows;
        //console.dir(objRet);
        return objRet.sort((a, b) => a.ISSUE_ID.localeCompare(b.ISSUE_ID));
    } catch (err) {
        console.log('Error occurred', err);
    } finally {
        if (conn) {
            try {
                await conn.close();
                console.log('Connection closed');
            } catch (err) {
                console.log('Error closing connection', err);
            }
        }
    }
}

