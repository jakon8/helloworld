const nodeCache = require("node-cache");
//import NodeCache from 'node-cache';

class CacheJK {

    constructor(ttlSeconds) {
        this.cache = new nodeCache({ stdTTL: ttlSeconds, checkperiod: ttlSeconds * 0.2, useClones: false });
    }

    get(key) {
        const value = this.cache.get(key);
        if (value) {
            return Promise.resolve(value);
        }
        return Promise.reject("Value not found");
    }

    get(key, storeFunction) {
        const value = this.cache.get(key);
        if (value) {
            return Promise.resolve(value);
        }

        return storeFunction().then((result) => {
            this.cache.set(key, result);
            return result;
        });
    }

    del(keys) {
        return this.cache.del(keys);
    }

    delStartWith(startStr = '') {
        if (!startStr) {
            return;
        }

        const keys = this.cache.keys();
        for (const key of keys) {
            if (key.indexOf(startStr) === 0) {
                this.del(key);
            }
        }
    }

    flush() {
        this.cache.flushAll();
    }

    keys() {
        return this.cache.keys();
    }

    set(key, val) {
        return this.cache.set(key, val);
    }

    set(key, val, ttl) {
        return this.cache.set(key, val, ttl);
    }
}


module.exports = CacheJK;