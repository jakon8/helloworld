const sd_providers = require('../tools/softdev/providers');
const providers = require('../tools/redmine/providers');

function getRedmineIssueLink(sd_issue, issues) {

    let sRet = '';
    issues.forEach((issue, i) => {
        issue.custom_fields.forEach((custom) => {
            //console.log(`Name: ${custom.name}, Value: ${custom.value}`);
            if (custom.name === 'SD Issue#' && custom.value === sd_issue) {
                sRet = process.env.REDMINE_PAGE + `/issues/${issue.id}`;
            }
        })
    })

    return sRet;
}

module.exports.getRegressionsIssues = async (regression) => {
    
    const sd_reg_issues = await sd_providers.getRegressionIssues(regression.sdPacketId, regression.sdPacket.endsWith('Packet') ? true : false);
    const issues = await providers.getIssuesInProject(regression.projectId);
    sd_reg_issues.display_created = regression.display_created;
    sd_reg_issues.forEach((sd_iss, i) => { 
        if (sd_iss !== undefined) {
            sd_iss.REDMINE_LINK = getRedmineIssueLink(sd_iss.ISSUE_ID, issues);
        }
    })

    return sd_reg_issues;
}