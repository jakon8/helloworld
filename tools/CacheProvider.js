const NodeCache = require("node-cache");
const providers = require("./redmine/providers");
const sd_providers = require("./softdev/providers");
const myCache = new NodeCache({ stdTTL: process.env.TTL, checkperiod: process.env.TTL * 0.2, useClones: false });

const key_projects = "redmine_projects";
const key_trackers = "redmine_trackers";
const key_people = "redmine_people";
const key_customFields = "redmine_customFields";
const key_project_memberships = "redmine_project_memberships";

const key_sd_projects = "sd_projects";

module.exports.getProjects = async (ttl = process.env.TTL) => {
    let cache_proj = myCache.get(key_projects);

    if (cache_proj === undefined) {
        cache_proj = await providers.getProjects();
        myCache.set(key_projects, cache_proj, ttl);
        //console.log("laduje kesz...");
    } else {
        //console.log("nie laduje kesza, bo juz jest...")
    }

    return cache_proj;
}

module.exports.getProjectMemberships = async (project_id, ttl = process.env.TTL) => {
    const proj_id = parseInt(project_id);
    if (isNaN(proj_id)) {
        return '';
    }

    if (proj_id < 0) {
        return await this.getUsers(process.env.UNLIMITED_TTL);
    }

    let key_proj_mem = key_project_memberships + proj_id;
    let cache_proj_mem = myCache.get(key_proj_mem);

    if (cache_proj_mem === undefined) {
        const users = await this.getUsers(process.env.UNLIMITED_TTL);
        cache_proj_mem = [];
        let basic_members = await providers.getProjectMemberships(proj_id);
        await basic_members.forEach(function (member) {
            const usr = users.find(function (user, index) {
                if (member.user && user.id === member.user.id)
                    return true;
            });

            usr ? cache_proj_mem.push(usr) : '';
        });
        myCache.set(key_proj_mem, cache_proj_mem.sort((a, b) => a.fullName.localeCompare(b.fullName)), ttl);
    }

    return cache_proj_mem;
}

module.exports.getTrackers = async (ttl = process.env.TTL) => {
    let cache = myCache.get(key_trackers);

    if (cache === undefined) {
        cache = await providers.getTrackers()
        myCache.set(key_trackers, cache, ttl);
        //console.log("laduje kesz...");
    } else {
        //console.log("nie laduje kesza, bo juz jest...")
    }

    return cache;
}

module.exports.getUsers = async (ttl = process.env.TTL) => {
    let cache = myCache.get(key_people);

    if (cache === undefined) {
        cache = await providers.getUsers();
        myCache.set(key_people, cache, ttl);
        //console.log("laduje kesz...");
    } else {
        //console.log("nie laduje kesza, bo juz jest...")
    }

    return cache;
}

module.exports.getCustomFields = async (ttl = process.env.TTL) => {
    let cache = myCache.get(key_customFields);

    if (cache === undefined) {
        cache = await providers.getCustomFields();
        myCache.set(key_customFields, cache, ttl);
    }

    return cache;
}

module.exports.getSDProjects = async (ttl = process.env.TTL) => {
    let cache = myCache.get(key_sd_projects);

    if (cache === undefined) {
        cache = await sd_providers.getSDProjects();
        myCache.set(key_sd_projects, cache, ttl);
    }

    return cache;
}

module.exports.clearCache = () => {
    myCache.flushAll();
}