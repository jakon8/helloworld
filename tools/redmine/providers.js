const axios = require('axios');

module.exports.getProjects = async () => {
    try {
        //const res = await axios.get(process.env.REDMINE_PAGE + '/projects.json' + '?key=' + process.env.REDMINE_API_KEY);
        let offset = 0;
        let limit = 100;
        let res = await axios.get(process.env.REDMINE_PAGE + `/projects.json?offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        let tmp = undefined;

        while (res.data.total_count > (offset + limit)) {
            offset = offset + limit;
            tmp = await axios.get(process.env.REDMINE_PAGE + `/projects.json?offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
            res.data.projects.push(...tmp.data.projects);
        }

        return res.data.projects.sort((a, b) => a.name.localeCompare(b.name));
    } catch (err) {
        console.log("Error! - getProjects problem!", err);
    }
}

module.exports.getProject = async (id) => {
    try {
        const res = await axios.get(process.env.REDMINE_PAGE + `/projects/${id}.json`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        return res.data.project;
    } catch (err) {
        console.log("Error! - getProject problem!", err);
    }
}

module.exports.getProjectMemberships = async (id) => {
    try {
        let offset = 0;
        let limit = 100;
        let res = await axios.get(process.env.REDMINE_PAGE + `/projects/${id}/memberships.json?offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        let tmp = undefined;

        while (res.data.total_count > (offset + limit)) {
            offset = offset + limit;
            tmp = await axios.get(process.env.REDMINE_PAGE + `/projects/${id}/memberships.json?offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
            res.data.memberships.push(...tmp.data.memberships);
        }

        return res.data.memberships;
    } catch (err) {
        console.log("Error! - getProjectMemberships problem!", err);
    }
}

module.exports.getTrackers = async () => {
    try {
        const res = await axios.get(process.env.REDMINE_PAGE + '/trackers.json', { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        return res.data.trackers.sort((a, b) => a.name.localeCompare(b.name));
    } catch (err) {
        console.log("Error! - getTrackers problem!", err);
    }
}

module.exports.getUsers = async () => {
    try {
        let offset = 0;
        let limit = 100;
        let res = await axios.get(process.env.REDMINE_PAGE + `/users.json?offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        let tmp = undefined;

        while (res.data.total_count > (offset + limit)) {
            offset = offset + limit;
            tmp = await axios.get(process.env.REDMINE_PAGE + `/users.json?offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
            res.data.users.push(...tmp.data.users);
        }

        let num = 1;
        res.data.users.forEach(function (element) {
            element.fullName = element.firstname + ' ' + element.lastname;
            element.ilosc = num;
            num = num + 1;
        });
        return res.data.users.sort((a, b) => a.fullName.localeCompare(b.fullName));
    } catch (err) {
        console.log("Error! - getUsers problem!", err);
    }
}

module.exports.getCustomFields = async () => {
    try {
        const res = await axios.get(process.env.REDMINE_PAGE + '/custom_fields.json', { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        return res.data.custom_fields.sort((a, b) => a.name.localeCompare(b.name));
    } catch (err) {
        console.log("Error! - getCustomFields problem!", err);
    }
}

module.exports.saveIssue = async (issueObj) => {
    let bRet = true;
    try {
        await axios.post(process.env.REDMINE_PAGE + '/issues.json', issueObj, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } })
            .then(function (response) {
                bRet = true;
            })
            .catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log("Response data: ", error.response.data);
                    console.log("Response status: ", error.response.status);
                    console.log("Response headers: ", error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log("Request error: ", error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
                bRet = false;
            });
    } catch (err) {
        console.log("saveIssue error: " + error.response.status);
        bRet = false;
    }

    return bRet;
}

module.exports.getIssuesInProject = async (project_id) => {
    try {
        let offset = 0;
        let limit = 100;
        let res = await axios.get(process.env.REDMINE_PAGE + `/issues.json?project_id=${project_id}&offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
        let tmp = undefined;

        while (res.data.total_count > (offset + limit)) {
            offset = offset + limit;
            tmp = await axios.get(process.env.REDMINE_PAGE + `/issues.json?project_id=${project_id}&offset=${offset}&limit=${limit}`, { headers: { 'X-Redmine-API-Key': process.env.REDMINE_API_KEY } });
            res.data.issues.push(...tmp.data.issues);
        }

        return res.data.issues;//.sort((a, b) => a.id.localeCompare(b.id));
    } catch (err) {
        console.log("Error! - getProjects problem!", err);
    }
}