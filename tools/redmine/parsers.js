const cacheProvider = require('../CacheProvider');

module.exports.prepareIssueJSON = async (baseJson) => {
    try {
        //const baseJson = JSON.parse(JSONstring);
        const customFields = await cacheProvider.getCustomFields(0);
        let crId, isId, tmId;
        let obj = customFields.find(x => x.name === "SD CR#");
        if (obj) crId = obj.id;
        obj = customFields.find(x => x.name === "SD Issue#");
        if (obj) isId = obj.id;
        obj = customFields.find(x => x.name === "TMS Task");
        if (obj) tmId = obj.id;
        const redmineIssueObj = {
            issue: {
                project_id: parseInt(baseJson.issue.projectId),
                tracker_id: parseInt(baseJson.issue.tracker),
                assigned_to_id: parseInt(baseJson.issue.assigneeId),
                subject: baseJson.issue.subject,
                description: baseJson.issue.description,
                custom_fields: [
                    {
                        value: baseJson.issue.sdIssue,
                        id: isId
                    },
                    {
                        value: baseJson.issue.sdCR,
                        id: crId
                    },
                    {
                        value: baseJson.issue.tms,
                        id: tmId
                    }
                ]
            }
        }
        return redmineIssueObj;
    } catch (ex) {
        console.log("prepareIssueJSON baseJson", baseJson);
        console.log("prepareIssueJSON error", ex);
    }
    return undefined;
}