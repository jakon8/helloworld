const cacheProvider = require('../tools/CacheProvider');

module.exports.renderSetupCache = (req, res) => {
    res.render(`setup/setup_cache`);
}

module.exports.clearCacheCt = (req, res) => {

    cacheProvider.clearCache();
    req.flash('success', 'Cache cleared!');
    res.redirect('/setup/setup_cache');
}