const cacheProvider = require('../tools/CacheProvider');
const parsers = require('../tools/redmine/parsers');
const providers = require('../tools/redmine/providers');
//const sd_providers = require('../tools/softdev/providers');
const regression_view_provider = require('../tools/RegressionViewDataPreparer');

//---------------------CONST--------------------------------

const RedmineProjectId = "RedmineProjectId";
const RedmineProjectIdentifier = "RedmineProjectIdentifier";
const RedmineProjectName = "RedmineProjectName";
const SDProjectId = "SDProjectId";
const SDProjectName = "SDProjectName";
const DisplayCreated = "DisplayCreated";

//-------------------END CONST------------------------------


//------------------CUSTOM ITEM------------------------------
module.exports.renderCustomItem = async (req, res, next) => {
    const project_cache = await cacheProvider.getProjects();
    const tracker_cache = await cacheProvider.getTrackers(process.env.UNLIMITED_TTL);
    const users_cache = await cacheProvider.getUsers(process.env.WEEK_TTL);
    const regression = undefined;

    res.render(`redmine/itemCreators/custom_item`, { project_cache, tracker_cache, users_cache, regression });
}

module.exports.getProjectMembers = async (req, res, next) => {
    const { proj_id } = req.params;
    const proj_members = await cacheProvider.getProjectMemberships(proj_id);
    res.send(JSON.stringify(proj_members));
}

module.exports.createCustomItem = async (req, res, next) => {

    const issueObj = await parsers.prepareIssueJSON(req.body);
    let flashObj = {key: '', value: ''};
    if (issueObj) {
        const sSaved = await providers.saveIssue(issueObj);
        if (sSaved) {
            flashObj.key = 'success';
            flashObj.value = 'New issue added to Redmine!';
        } else {
            flashObj.key = 'error';
            flashObj.value = 'Error! Issue not saved!';
        }
    } else {
        flashObj.key = 'error';
        flashObj.value = 'Error! Issue not saved!';
    }

    const { regression = undefined } = req.body;
    if (regression) {
        regression.flashObj = flashObj;
        return next();
    } else {
        req.flash(flashObj.key, flashObj.value);
        res.redirect('/itemcreators/custom_item');
    }
};

module.exports.createNewRegressionItem = async (req, res, next) => {

    const project_cache = await cacheProvider.getProjects();
    const tracker_cache = await cacheProvider.getTrackers(process.env.UNLIMITED_TTL);
    const users_cache = await cacheProvider.getUsers(process.env.WEEK_TTL);
    let { regression } = req.body;
    let project = project_cache.find((project) => {
        return project.identifier === regression.project_identifier;
    });
    regression.projectId = project.id;
    
    res.render(`redmine/itemCreators/custom_item`, { project_cache, tracker_cache, users_cache, regression });
}

//-----------------------REGRESSION ITEM-------------------------------

module.exports.renderRegressionItem = async (req, res, next) => {
    const project_cache = await cacheProvider.getProjects();
    const sd_project_cache = await cacheProvider.getSDProjects();
    const sd_reg_issues = undefined;
    const reg_cookies = req.signedCookies;

    res.render(`redmine/itemCreators/regression_item`, { project_cache, sd_project_cache, sd_reg_issues, reg_cookies});
}

module.exports.createRegressionItem = async (req, res, next) => {

    const { regression } = req.body;
    if (regression.flashObj) {
        //req.flash(flashObj.key, flashObj.value);
    }
    const project_cache = [{id: regression.projectId, identifier: regression.project_identifier, name: regression.project}];
    const sd_project_cache = [{PRODUCT_VERSION_ID: regression.sdPacketId, PRODUCT_VERSION_NAME: regression.sdPacket}];
    const reg_cookies = undefined;

    res.cookie(RedmineProjectId, regression.projectId, { signed: true });
    res.cookie(RedmineProjectIdentifier, regression.project_identifier, { signed: true });
    res.cookie(RedmineProjectName, regression.project, { signed: true });
    res.cookie(SDProjectId, regression.sdPacketId, { signed: true });
    res.cookie(SDProjectName, regression.sdPacket, { signed: true });
    res.cookie(DisplayCreated, regression.display_created, { signed: true });

    const sd_reg_issues = await regression_view_provider.getRegressionsIssues(regression);

    res.render(`redmine/itemCreators/regression_item`, { project_cache, sd_project_cache, sd_reg_issues, reg_cookies});
}

