const ExpressError = require("../../tools/ExpressError");
const cacheProvider = require('../../tools/CacheProvider');
const { issueSchema, regressionSchema } = require('../../tools/JoiSchemas');
const { boolean } = require("joi");

//pattern="^(<%= project_cache.map(p => p.name).join("|")) %>)$"

module.exports.validateIssue = async (req, res, next) => {
    const bError = false;
    const { error } = issueSchema.validate(req.body);
    if (error) {
        const msg = error.details.map(x => x.message).join(',');
        req.flash('error', msg);
        res.redirect(req.originalUrl);
        bError = true;
    }

    const project_cache = await cacheProvider.getProjects();
    if (!project_cache.some(proj => proj.name === req.body.issue.project)) {
        req.flash('error', "Wrong project name");
        res.redirect(req.originalUrl);
        bError = true;
    }

    const users_cache = await cacheProvider.getUsers(60 * 60 * 24 * 7);
    if (!users_cache.some(user => user.fullName === req.body.issue.assignee)) {
        req.flash('error', "Wrong user name");
        res.redirect(req.originalUrl);
        bError = true;
    }

    const tracker_cache = await cacheProvider.getTrackers(0);
    if (!tracker_cache.some(tracker => tracker.id === parseInt(req.body.issue.tracker))) {
        req.flash('error', "Wrong tracker");
        res.redirect(req.originalUrl);
        bError = true;
    }
    
    if (!bError) {
        next();
    }
};

module.exports.validateRegression = async (req, res, next) => {
    const bError = false;
    const { error } = regressionSchema.validate(req.body);
    if (error) {
        const msg = error.details.map(x => x.message).join(',');
        req.flash('error', msg);
        res.redirect(req.originalUrl);
        bError = true;
    }

    const project_cache = await cacheProvider.getProjects();
    if (!project_cache.some(proj => proj.name === req.body.regression.project)) {
        req.flash('error', "Wrong project name");
        res.redirect(req.originalUrl);
        bError = true;
    }

    const sd_project_cache = await cacheProvider.getSDProjects();
    if (!sd_project_cache.some(sd_project => sd_project.PRODUCT_VERSION_NAME === req.body.regression.sdPacket)) {
        req.flash('error', "Wrong SoftDev project");
        res.redirect(req.originalUrl);
        bError = true;
    }
    
    if (!bError) {
        next();
    }
};