//-----------------PREPARE PROJECT MEMBERS--------------------------------

function prepareProjAssignee(input) {
    console.log("Jestem w srodku");
    let hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');
    const id = parseInt(hiddenInput.value);
    console.log("Hidden input value: " + id);
    if (input.value.length > 0 && id >= 0) {
        let assignee = document.querySelector("#assignee");
        if (assignee) {
            assignee.value = '';
        }
        loadProjectMembers(id);
    } else {
        loadProjectMembers(-1);
    }
}

async function loadProjectMembers(id) {
    let cList = document.querySelector("#assigneeDatalist");

    if (cList) {
        let res = await axios.get(`http://localhost:3000/itemcreators/custom_item/${id}`);
        const projMembers = res.data;
        if (projMembers) {
            var children = [...cList.children];
            children.forEach(c => c.remove());
            projMembers.forEach(m => {
                var newChild = document.createElement("option");
                newChild.textContent = m.fullName;
                newChild.value = m.login;
                newChild.id = m.id;
                cList.appendChild(newChild);
            })
        }
    }
}

//-----------------END PREPARE PROJECT MEMBERS--------------------------------


//-----------------SHOW/HIDE GRID ROWS--------------------------------------------

function showHideRow(check) {
    if (check) {
        rows = document.getElementsByName("hiddenRegRow");
        rows.forEach((row) => {
            if ((check.checked && row.classList.contains("hiddenRow")) ||
                (!check.checked && !row.classList.contains("hiddenRow"))) {
                row.classList.toggle("hiddenRow");
            }
        })
    }
}

//-----------------END SHOW/HIDE GRID ROWS--------------------------------------------