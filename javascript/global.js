
function changeTextFont(val) {
    const mySelect = document.getElementById('display_fonts');
    if (mySelect) {
        const myTextDiv = document.getElementById('helloWorldText');
        if (myTextDiv) {
            myTextDiv.innerHTML = 'Hello World ' + `[${val}]`;
            switch (mySelect.value) {
                case 'circle':
                    myTextDiv.innerHTML = 'Hello World ' + `[${val}] AA`;
                    break;
                default:

            }
        }
    }
    //return e.target.value;
}

//circle menu
function circleMenuActive(menu) {
    document.getElementById(menu).classList.toggle('active');
}
//-----END circle menu 


//------------------BOOTSTRAP MENU------------------------------
const click_submenu = function (e) {
    let nextEl = this.nextElementSibling;
    //console.log(e);
    if (nextEl && nextEl.classList.contains('jk_smd')) {
        // prevent opening link if link needs to open dropdown
        if (window.innerWidth < 768) {
            e.preventDefault();
            if (nextEl.style.display == 'block') {
                //console.log("bylo block i ustawiam none");
                nextEl.style.display = 'none';
            } else {
                nextEl.style.display = 'block';
            }
        } else {
            //console.log("else: " + nextEl.style.display);
            if (nextEl.style.display == '' || nextEl.style.display == 'block') {
                nextEl.style.display = 'none';
            } else {
                nextEl.style.display = '';
            }
        }
    }
}

const click_menu = function (e) {
    let nextEl = this.nextElementSibling;
    //console.log(e);
    if (nextEl && nextEl.classList.contains('jk_md')) {
        // prevent opening link if link needs to open dropdown
        if (window.innerWidth < 768) {
            e.preventDefault();
            if (nextEl.style.display == 'block') {
                nextEl.style.display = 'none';
            } else {
                nextEl.style.display = 'block';
            }
        } else {
            //console.log("else: " + nextEl.style.display);
            if (nextEl.style.display == '' || nextEl.style.display == 'block') {
                nextEl.style.display = 'none';
            } else {
                nextEl.style.display = '';
            }
        }
    }
}

function reloadCss() {
    var links = document.getElementsByTagName("link");
    for (var cl in links) {
        var link = links[cl];
        if (link.rel === "stylesheet")
            link.href += "";
    }
}

document.addEventListener("DOMContentLoaded", function () {
    window.addEventListener('resize', function (event) {
        if (window.innerWidth < 768) {
            document.querySelectorAll('.jk_smn a').forEach(function (element) {
                if (element.getAttribute('jk_click_submenu') !== 'true') {
                    element.setAttribute("jk_click_submenu", 'true');
                    element.addEventListener('click', click_submenu);
                }
            })

            document.querySelectorAll('.jk_mn a').forEach(function (element) {
                if (element.getAttribute('jk_click_menu') !== 'true') {
                    element.setAttribute("jk_click_menu", 'true');
                    element.addEventListener('click', click_menu);
                }
            })
        } else {
            //reloadCss();
            document.querySelectorAll('.jk_smn a').forEach(function (element) {
                if (element.getAttribute('jk_click_submenu') === 'true') {
                    let nextEl = this.nextElementSibling;
                    //console.log(e);
                    if (nextEl && nextEl.classList.contains('jk_smd')) {
                        nextEl.style.display = '';
                    }
                    element.setAttribute("jk_click_submenu", 'false');
                    element.removeEventListener('click', click_submenu);
                }
            })

            document.querySelectorAll('.jk_mn a').forEach(function (element) {
                if (element.getAttribute('jk_click_menu') === 'true') {
                    let nextEl = this.nextElementSibling;
                    //console.log(e);
                    if (nextEl && nextEl.classList.contains('jk_md')) {
                        nextEl.style.display = '';
                    }
                    //element.style.display = '';
                    //element.setAttribute("jk_click_menu", 'false');
                    //element.removeEventListener('click', click_menu);
                }
            })
        }
    });

    // make it as accordion for smaller screens
    if (window.innerWidth < 768) {
        document.querySelectorAll('.jk_smn a').forEach(function (element) {
            element.setAttribute("jk_click_submenu", 'true');
            element.addEventListener('click', click_submenu);
        })

        document.querySelectorAll('.jk_mn a').forEach(function (element) {
            element.setAttribute("jk_click_menu", 'true');
            element.addEventListener('click', click_menu);
        })
    }
    // end if innerWidth
});
// DOMContentLoaded  end

//------------------END BOOTSTRAP MENU------------------------------



//-----------------VALIDATE DATALIST--------------------------------

function is_valid_datalist_value(idDataList, inputValue) {

    var option = document.querySelector("#" + idDataList + " option[value='" + inputValue + "']");
    if (option != null) {
        return option.value.length > 0;
    }
    return false;
}

function prepareIdForDataList(input) {

    const list = input.getAttribute('list');
    const options = document.querySelectorAll('#' + list + ' option[value="' + input.value + '"]');
    let hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');

    if (options.length > 0) {
        hiddenInput.value = options[0].id;
        input.value = options[0].innerText;
    }
}

function doValidateDataList(input) {

    if (input && is_valid_datalist_value(input.list.id, input.value)) {
        prepareIdForDataList(input);
        input.setCustomValidity('');
        return true;
    } else {
        input.setCustomValidity('error');
        return false;
    }
}

//-----------------END VALIDATE DATALIST--------------------------------

//-----------------PREPARE PROJECT IDENTIFIER---------------------------

function prepareProjIdentifierFromDataList(input) {
    
    if (input) {
        const hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');
        const list = input.getAttribute('list');
        if (hiddenInput && list) {
            const options = document.querySelectorAll('#' + list + ' option[id="' + hiddenInput.value + '"]');
            let hiddenIdentifierInput = document.getElementById(input.getAttribute('id') + '-Identifier_hidden');

            if (options && options.length > 0 && hiddenIdentifierInput) {
                hiddenIdentifierInput.value = options[0].value;
            }
        }
    }
}

//-----------------END PREPARE PROJECT IDENTIFIER------------------------

//-----------------SHOW LOGIN PAGE--------------------------------------------

function showLoginPopup() {
    Swal.fire({
        title: 'Login Form',
        html: `<input type="text" id="login" class="swal2-input" placeholder="Username">
        <input type="password" id="password" class="swal2-input" placeholder="Password">`,
        confirmButtonText: 'Sign in',
        focusConfirm: false,
        preConfirm: () => {
            const login = Swal.getPopup().querySelector('#login').value
            const password = Swal.getPopup().querySelector('#password').value
            if (!login || !password) {
                Swal.showValidationMessage(`Please enter login and password`)
            }
            return { login: login, password: password }
        }
    }).then((result) => {
        Swal.fire(`
          Login: ${result.value.login}
          Password: ${result.value.password}
        `.trim())
    })
}

//-----------------END SHOW LOGIN PAGE--------------------------------------------

//-----------------FLASH MESSAGES--------------------------------------------



//-----------------END FLASH MESSAGES--------------------------------------------