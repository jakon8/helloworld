require("./configuration/environmentConfig").loadEnvConfig();

const express = require("express");
const app = express();

const libsAndServers = require("./configuration/libsAndServers");
const routings = require("./configuration/routingLoader");

libsAndServers.loadsAndConfigs(app, __dirname)
routings.loadRoutigs(app);

libsAndServers.startServer(app);

